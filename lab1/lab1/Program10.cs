﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{
    class Program11
    {

    }

    interface Shape
    {
        void Draw();
    }

    class Circle : Shape
    {
        public void Draw()
        {
           // Console.WriteLine("Drawing a circle");
        }
    }

    class Rectangle : Shape
    {
        public void Draw()
        {
            Console.WriteLine("Drawing a rectangle");
        }
    }

   

}
