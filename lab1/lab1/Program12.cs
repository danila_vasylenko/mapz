﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{
    class Program13 : Object
    {
        private int a;
        public Program13(int a)
        {
            this.a = a;
        }

        public override bool Equals(object otherObject)
        {
            if (!(otherObject is Program13) || otherObject == null)
            {
                return false;
            }
            Program13 prog = (Program13)otherObject;
            return a == prog.a;
        }
        public override int GetHashCode()
        {
            return this.a.GetHashCode();
        }
        public override string ToString()
        {
            return $"X={this.a}";
        }

        
    }
}
