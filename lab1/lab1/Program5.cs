﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{

    interface A1{
        void g();
    }

    interface A2
    {
        void h();
    }
    class Program5 : A1, A2
    {
        public void g() { }
        public void h() { }
    }
}
