﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{

    class Human{
        public static int a1;
        private int b;

        static Human()
        {
            a1 = 3;
            Console.WriteLine("first");
        }
        public Human(int n)
        {
            this.b = n;
            Console.WriteLine("second");
        }
        public Human(int a,int b) : this(b)
        {
            Human.a1 = a;
            Console.WriteLine("third");
        }


    }

    class Program6
    {
        
    }
}
