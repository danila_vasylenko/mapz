﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{
    class Program9
    {
        private int number;

        public Program9(int number)
        {
            this.number = number;
        }
        public static implicit operator int(Program9 myInt)=>myInt.number;
        public static explicit operator Program9(int number) => new Program9(number);
    }
}
