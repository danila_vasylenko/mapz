﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace lab1
{
    class Program10
    {
        public class Animal1
        {
            public virtual void MakeSound()
            {

            }
        }
        public class Dog1 : Animal1
        {
            public override void MakeSound()
            {
                //Console.WriteLine("Gaw.");
            }
        }
        public class Cat1 : Animal1
        {
            public override void MakeSound()
            {
                Console.WriteLine("Meow");
            }
        }

        public class Animal
        {
            public void MakeSound()
            {

            }
        }
        public class Dog : Animal
        {
            public void MakeSound()
            {
               // Console.WriteLine("Gaw.");
            }
        }
        public class Cat : Animal
        {
            public void MakeSound()
            {
                Console.WriteLine("Meow");
            }
        }

        static void Main(string[] args)
        {
            //int k = 10;
            //Object ob = k;
            //Console.WriteLine(ob);
            //k = (int)ob;
            //Console.WriteLine(ob);
            //int a2=10, b;
            //Program8 o = new Program8();
            //o.plus(ref a2);
            //o.minus(out b);
            //Console.WriteLine(a2 + " " + b);
            //int a1 = 10;
            //Human ob1 = new Human(a1,20);
            Stopwatch stopwatch = new Stopwatch();
            Animal1[] animal = new Animal1[1];
            animal[0] = new Dog1();
            stopwatch.Start();
            for (int i = 0; i < 10000000; i++)
            {
                animal[0].MakeSound();
            }
            stopwatch.Stop();
            Console.WriteLine($"Time elapsed using virtual methods: {stopwatch.ElapsedMilliseconds} ms");

            Animal[] a = new Animal[1];
            a[0] = new Dog();
            stopwatch.Restart();
            for (int i = 0; i < 10000000; i++)
            {
                Type type = a[0].GetType();
                MethodInfo method = type.GetMethod("MakeSound");
                method.Invoke(a[0], null);
            }
            stopwatch.Stop();
            Console.WriteLine($"Time elapsed using reflection methods: {stopwatch.ElapsedMilliseconds} ms");

            Shape[] ob = new Shape[1];
            ob[0] = new Circle();
            stopwatch.Restart();
            for (int i = 0; i < 10000000; i++)
            {
                ob[0].Draw();
            }
            stopwatch.Stop();
            Console.WriteLine($"Time elapsed using interface methods: {stopwatch.ElapsedMilliseconds} ms");
            //Animal[] animals = new Animal[2];
            //animals[0] = new Dog();
            //animals[1] = new Cat();
            //foreach (Animal animal in animals)
            //{
            //    animal.MakeSound();
            //}
            //foreach (Animal animal in animals)
            //{
            //    Type type = animal.GetType();
            //    MethodInfo method = type.GetMethod("MakeSound");
            //    method.Invoke(animal, null);
            //}
            //foreach (Animal animal in animals)
            //{
            //    animal.MakeSound();
            //}
            //IShape[] shapes = new IShape[2];
            //shapes[0] = new Circle();
            //shapes[1] = new Rectangle();

            //foreach (IShape shape in shapes)
            //{
            //    shape.Draw();
            //}
            //Program13 ob = new Program13(100);
            //Console.WriteLine(ob.Equals(10));
            //Console.WriteLine(ob.GetHashCode());
            //Console.WriteLine(ob.GetType());
            //Console.WriteLine(ob.ToString());
        }
    }
}
